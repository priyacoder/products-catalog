package com.android.priyanallan.check24productscatalog.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.android.priyanallan.check24productscatalog.model.ProductTest
import com.android.priyanallan.check24productscatalog.remote.RetrofitService
import com.android.priyanallan.check24productscatalog.utils.Constants
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ProductsViewModel : ViewModel() {
    var productsObservable: MutableLiveData<ProductTest> = MutableLiveData()

    init {
        getProducts()
    }

    fun getProducts() {
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(RetrofitService::class.java)

        service.getProducts().enqueue(object : Callback<ProductTest> {
            override fun onFailure(call: Call<ProductTest>, t: Throwable) {
                Log.e("ProductsActivity", "Error in retrofit ${t.localizedMessage}")
            }

            override fun onResponse(
                call: Call<ProductTest>,
                response: Response<ProductTest>
            ) {
                productsObservable.value = response.body()
            }

        })

    }
}