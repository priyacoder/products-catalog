package com.android.priyanallan.check24productscatalog.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Price (
    @SerializedName("currency")
    val currency: String,
    @SerializedName("value")
    val value: Double
): Serializable