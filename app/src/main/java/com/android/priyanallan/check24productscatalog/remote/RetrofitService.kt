package com.android.priyanallan.check24productscatalog.remote

import com.android.priyanallan.check24productscatalog.model.ProductTest
import retrofit2.Call
import retrofit2.http.GET

interface RetrofitService {

    @GET("products-test.json")
    fun getProducts(): Call<ProductTest>
}