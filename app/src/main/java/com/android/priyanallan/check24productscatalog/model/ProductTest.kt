package com.android.priyanallan.check24productscatalog.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ProductTest(

    @SerializedName("header") val header: Header,
    @SerializedName("filters") val filters: List<String>,
    @SerializedName("products") val productTests: List<Product>
) : Serializable