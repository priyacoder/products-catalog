package com.android.priyanallan.check24productscatalog.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Header(
    @SerializedName("headerTitle")
    val title: String,
    @SerializedName("headerDescription") val subtitle: String
): Serializable