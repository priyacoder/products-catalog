package com.android.priyanallan.check24productscatalog

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Gravity
import android.widget.LinearLayout
import android.widget.TextView
import com.android.priyanallan.check24productscatalog.adapter.ProductsAdapter
import com.android.priyanallan.check24productscatalog.model.ProductTest
import com.android.priyanallan.check24productscatalog.viewmodel.ProductsViewModel
import kotlinx.android.synthetic.main.activity_main.*

class ProductsActivity : AppCompatActivity() {

    lateinit var productsViewModel: ProductsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        productsList.layoutManager = LinearLayoutManager(this)

        productsViewModel = ViewModelProviders.of(this).get(ProductsViewModel::class.java)

        productsViewModel.productsObservable.observe(this, object : Observer<ProductTest> {
            override fun onChanged(productTest: ProductTest?) {
                productTest?.apply {
                    tv_title.text = header.title
                    tv_subtitle.text = header.subtitle

//                    showFilters()

                    productsList.adapter = ProductsAdapter(this.productTests)
                }
            }

        })
    }

    private fun ProductTest.showFilters() {
        if (filters.size > 0) {
            val layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            layoutParams.gravity = Gravity.BOTTOM
            val textView = TextView(this@ProductsActivity)
            textView.layoutParams = layoutParams
            textView.text = filters[0]

            addContentView(textView, layoutParams)
        }
    }
}
