package com.android.priyanallan.check24productscatalog.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Product(
    @SerializedName("name") val name: String,
    @SerializedName("description") val description: String,
    @SerializedName("imageURL") val imageUrl: String,
    @SerializedName("available") val available: Boolean,
    @SerializedName("rating") val rating: Double,
    @SerializedName("price") val price: Price,
    @SerializedName("releaseDate") val releaseDate: Long
): Serializable