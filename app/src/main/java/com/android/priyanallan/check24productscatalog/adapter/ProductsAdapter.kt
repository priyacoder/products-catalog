package com.android.priyanallan.check24productscatalog.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.android.priyanallan.check24productscatalog.R
import com.android.priyanallan.check24productscatalog.model.Product
import kotlinx.android.synthetic.main.list_item_product.view.*
import java.text.SimpleDateFormat
import java.util.*

class ProductsAdapter(products: List<Product>) : RecyclerView.Adapter<ProductsAdapter.ViewHolder>() {

    val productsList = products
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val layout = LayoutInflater.from(p0.context).inflate(R.layout.list_item_product, p0, false)
        return ViewHolder(layout)
    }

    override fun getItemCount(): Int {
        return productsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val product = productsList.get(position) as Product

        holder.itemView.apply {
            productName.text = product.name
            productDesc.text = product.description
            price.text = product.price.value.toString()
            currency.text = product.price.currency
            rating.rating = product.rating.toFloat()
            setImage(productImage, product.imageUrl)

            releaseDate.text = getDate(product.releaseDate)

        }
    }


    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

    fun setImage(imageView: ImageView, imageUrl: String) {

        Glide.with(imageView)
            .load(imageUrl)
            .centerCrop()
            .placeholder(android.R.drawable.sym_def_app_icon)
            .into(imageView)
    }

    fun getDate(date: Long): String {
        return SimpleDateFormat("MM.dd.YYYY").format(Date(date * 1000))
    }
}